import java.lang.reflect.Array;

public class FibonacciGenerator {
	
private int m=0;
String n=null;

	public FibonacciGenerator(String in) {
			n = in;			
	}
	
	
	public boolean validateN() throws Exception  {
		
		try {
		       m=Integer.parseInt(n);
		       
		       return true;
		       
	      }  catch(Exception e) {
	    	 return false;
	       	
	    }
		
	      
	}
	
	
	public int calculateNth() throws Exception{
		try {
		       m=Integer.parseInt(n);		      
		       
	      }  catch(Exception e) {
	    	 
	       	
	    }
		return calculateNth(m);
	}
	
	
	public int calculateNth(int m) {		
			
		if (m <= 1) 
		       return m; 
		else
		    return calculateNth(m-1) + calculateNth(m-2); 
		
	}
}
