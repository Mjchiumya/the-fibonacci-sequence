import java.util.Scanner;

public class RunTFS {
	
	 
	public static void main(String[] args) throws Exception{
		
		String input;
		
		System.out.println("Please enter number to find its position on the Fibonnacci sequence");
		Scanner scan = new Scanner(System.in);
		
		
	
		do {			
			input = scan.next();	
			FibonacciGenerator fg = new FibonacciGenerator(input);			
			System.out.println("Option Entered is :"+input );
			System.out.println("Number validation :"+fg.validateN() );
			System.out.println("Number is the "+fg.calculateNth()+"nth on the Fibonacci No Sequence" );
			System.out.println("Enter another Number");
			
		}while(scan.hasNext());
		
		
	}
}
